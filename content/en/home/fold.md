---
title: "The Fold"
headless: true
---

The Fold is a theory, artificial intelligence, and video game that will be used to rehabilitate the multiverse.

[thefold.io](https://thefold.io)