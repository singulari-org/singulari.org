---
title: "The Cults"
headless: true
weight: 10
---

The following groups are responsible for the development, funding, and success of the Fold:

[22sierracoffee.com](https://22sierracoffee.com/)

[4chan.org](https://www.4chan.org/)

[973-eht-namuh-973.com](http://973-eht-namuh-973.com/)

[acultofone.com](http://acultofone.com)

[ae911truth.org](https://www.ae911truth.org/)

[aiahouston.org](https://aiahouston.org/v/texas-architects/)

[amazon.com](https://aws.amazon.com/)

[amd.com](https://www.amd.com/)

[americastestkitchen.com](https://www.americastestkitchen.com/)

[amerisurg.com](https://www.amerisurg.com/)

[anandtech.com](https://www.anandtech.com/)

[apache.org](https://www.apache.org/)

[aquapools.com](https://www.aquapools.com/)

[austincc.edu](https://www.austincc.edu/)

[asq.org](https://asq.org/)

[atheist-community.org](https://atheist-community.org/)

[atlassian.com](https://www.atlassian.com/)

[australia.gov.au](https://www.australia.gov.au/)

[aventiv.com](https://www.aventiv.com/)

[aventivresearch.com](https://www.aventivresearch.com/)

[axelos.com](https://www.axelos.com/)

[babbel.com](https://www.babbel.com/)

[batsov.com](https://batsov.com/)

[bbrfoundation.org](https://www.bbrfoundation.org/)

[beefjerkyoutlet.com](https://www.beefjerkyoutlet.com/)

[believeinbalance.com](http://believeinbalance.com/)

[bentoandstarchky.com](http://www.bentoandstarchky.com/)

[berkshirehathaway.com](https://www.berkshirehathaway.com/)

[bilderbergmeetings.org](https://bilderbergmeetings.org/index.html)

[bioware.com](https://www.bioware.com/)

[blizzard.com](https://www.blizzard.com/)

[caligulashorse.com](https://caligulashorse.com/)

[captaindisillusion.com](http://captaindisillusion.com/)

[carlaspradbery.com](http://www.carlaspradbery.com/)

[centerforinquiry.org](https://centerforinquiry.org/)

[chef.io](https://www.chef.io/)

[cia.gov](https://www.cia.gov)

[cisco.com](https://www.cisco.com/)

[clintonfoundation.org](https://www.clintonfoundation.org/)

[cloudyfox.io](https://www.cloudyfox.io/)

[comradef.com](http://www.comradef.com/)

[connectwise.com](https://www.connectwise.com/)

[covenanteyes.com](https://www.covenanteyes.com/)

[crowbarcollective.com](https://www.crowbarcollective.com/)

[crowdstrike.com](https://www.crowdstrike.com/)

[csosa.gov](https://www.csosa.gov/)

[cyberdefenses.com](https://cyberdefenses.com/)

[dallaspolice.net](https://dallaspolice.net/)

[dancarlin.com](https://www.dancarlin.com/)

[dansville.org](https://www.dansville.org/)

[darklyrics.com](http://www.darklyrics.com/)

[dartcontainer.com](https://www.dartcontainer.com/)

[davidcsmalley.com](https://www.davidcsmalley.com/)

[db.com](https://www.db.com/)

[defense.gov](https://www.defense.gov/)

[dell.com](https://www.dell.com/)

[deloitte.com](https://www2.deloitte.com/)

[delta.com](https://www.delta.com/)

[deviantart.com](https://www.deviantart.com/)

[discord.com](https://discord.com/)

[docker.com](https://www.docker.com/)

[dollarshaveclub.com](https://www.dollarshaveclub.com/)

[dominos.com](https://www.dominos.com/)

[donaldjtrump.com](https://www.donaldjtrump.com/)

[dpr.com](https://www.dpr.com/)

[dreamquest.org](https://dreamquest.org/)

[dwavesys.com](https://www.dwavesys.com/)

[eff.org](https://www.eff.org/)

[elastic.co](https://www.elastic.co/)

[endhomelessness.org](https://endhomelessness.org/)

[epds.telangana.gov.in](https://epds.telangana.gov.in/FoodSecurityAct/)

[ephemeralrift.com](https://ephemeralrift.com/)

[expressvpn.com](https://www.expressvpn.com/)

[evolveum.com](https://evolveum.com/)

[f42home.com](http://f42home.com/)

[facebook.com](https://www.facebook.com/)

[fbi.gov](https://www.fbi.gov/)

[ffrf.org](https://ffrf.org/)

[firaxis.com](https://firaxis.com/)

[firstbook.org](https://firstbook.org/)

[fluentco.com](https://www.fluentco.com/)

[flyingferretproductions.com](http://flyingferretproductions.com/)

[freedomfortargetedindividuals.org](https://www.freedomfortargetedindividuals.org/)

[futel.net](http://futel.net)

[g-omedia.com](https://g-omedia.com/)

[games-workshop.com](https://www.games-workshop.com/)

[gatesfoundation.org](https://www.gatesfoundation.org/)

[gemini.com](https://gemini.com/)

[gitlab.com](https://gitlab.com/)

[glanet.org](https://glanet.org/)

[glassesusa.com](https://www.glassesusa.com/)

[glonass-iac.ru](https://www.glonass-iac.ru/)

[gohugo.io](https://gohugo.io/)

[goodbyyou.carrd.co](https://goodbyyou.carrd.co/)

[google.com](https://www.google.com/)

[gop.com](https://www.gop.com/)

[government.ru](http://government.ru/)

[grafana.com](https://grafana.com/)

[graywalkerinc.com](https://www.graywalkerinc.com/)

[grc.com](https://www.grc.com/)

[growthproject.org](http://www.growthproject.org/)

[Happy Valley Dream Survey](https://www.youtube.com/watch?v=LlbiuLiU9K8)

[hashicorp.com](https://www.hashicorp.com/)

[hatrack.com](http://www.hatrack.com/)

[hekatestation.net](http://hekatestation.net/)

[helixsleep.com](https://helixsleep.com/)

[hewillnotdivide.us](http://www.hewillnotdivide.us/)

[hollowpoint.org](https://hollowpoint.org/)

[houstontx.gov](https://www.houstontx.gov/)

[hover.com](https://www.hover.com/)

[hp.com](https://www8.hp.com/)

[htc.com](https://www.htc.com/)

[huawei.com](https://www.huawei.com/)

[hubspot.com](https://www.hubspot.com/)

[identityautomation.com](https://www.identityautomation.com/)

[interpol.int](https://www.interpol.int/)

[John D. Barkham](https://www.eduinreview.com/scholarships/john-d.-barkham-expendable-fund-in-the-school-of-hospitality-business-scholarship-71830)

[ikea.com](https://www.ikea.com/)

[incompetech.com](https://incompetech.com/)

[inklestudios.com](https://www.inklestudios.com/)

[intel.com](https://www.intel.com/)

[itpro.tv](https://www.itpro.tv/)

[jacobs.com](https://www.jacobs.com/)

[jeffreyepstein.org](http://www.jeffreyepstein.org/)

[joerogan.com](https://www.joerogan.com/)

[joinhoney.com](https://www.joinhoney.com/)

[jordanbpeterson.com](https://www.jordanbpeterson.com/)

[judyrecords.com](https://www.judyrecords.com/)

[justice.gov](https://www.justice.gov/)

[kanyewest.com](https://www.kanyewest.com/)

[kickstarter.comF](https://www.kickstarter.com/)

[lanl.gov](https://www.lanl.gov/)

[larian.com](https://larian.com/)

[lbry.tv](https://lbry.tv/)

[lcc.edu](https://lcc.edu/)

[lenovo.com](https://www.lenovo.com/)

[linuxfoundation.org](https://www.linuxfoundation.org/)

[maio-architects.com](https://www.maio-architects.com/)

[marines.com](https://www.marines.com/)

[mastodon.social](https://mastodon.social/)

[medienkunstnetz.de](http://www.medienkunstnetz.de/)

[mega.nz](https://mega.nz/)

[met.police.uk](https://www.met.police.uk/)

[metallica.com](https://www.metallica.com/)

[microsoft.com](https://www.microsoft.com/)

[mit.edu](https://www.mit.edu/)

[modcon-systems.com](https://www.modcon-systems.com/)

[mojang.com](https://www.mojang.com/)

[moonsociety.org](https://www.moonsociety.org/)

[mossfon.com](http://mossfon.com/)

[Mouthy Buddha](https://www.youtube.com/channel/UCKEt1xKVBLuL175dkk8rqLg)

[movehumanityforward.com](https://movehumanityforward.com/)

[mtb.com](https://www3.mtb.com/)

[mtbakervapor.com](https://www.mtbakervapor.com/)

[mtsweb.net](https://www.mtsweb.net/)

[naidoc.org.au](https://www.naidoc.org.au/get-involved/2019-theme)

[nasa.gov](https://www.nasa.gov/)

[nato.int](https://www.nato.int/)

[navy.mil](https://www.navy.mil/)

[netcorecloud.com](https://netcorecloud.com/)

[netflix.com](https://www.netflix.com/)

[neuralink.com](https://www.neuralink.com/)

[nist.gov](https://www.nist.gov/)

[nordvpn.com](https://nordvpn.com/)

[norml.org](https://norml.org/)

[nsa.gov](https://www.nsa.gov/)

[nvidia.com](https://www.nvidia.com/)

[obama.org](https://www.obama.org/)

[obscuraantiques.com](https://www.obscuraantiques.com/)

[obsidian.net](https://www.obsidian.net/)

[okcupid.com](https://www.okcupid.com/)

[openai.com](https://openai.com/)

[openvpn.net](https://openvpn.net/)

[oracle.com](https://www.oracle.com/)

[ornl.gov](https://www.ornl.gov/)

[paradoxinteractive.com](https://www.paradoxinteractive.com/)

[patreon.com](https://www.patreon.com/)

[pornhub.com](https://www.pornhub.com/)

[postgresql.org](https://www.postgresql.org/)

[prometheus.io](https://prometheus.io/)

[proxpn.com](https://secure.proxpn.com/)

[pyramidrealm.com](https://pyramidrealm.com/)

[quantcast.com](https://www.quantcast.com/)

[randonautica.com](https://www.randonautica.com/)

[rayconglobal.com](https://rayconglobal.com/)

[research.ibm.com](https://www.research.ibm.com/)

[reddit.com](https://www.reddit.com/)

[reddit.com/r/5September2020](https://www.reddit.com/r/5September2020/)

[reddit.com/r/AlphaCandidate](https://www.reddit.com/r/AlphaCandidate/)

[reddit.com/r/Backlands](https://www.reddit.com/r/Backlands/)

[reddit.com/r/Frontlands](https://www.reddit.com/r/Frontlands/)

[reddit.com/r/SARAHAI](https://www.reddit.com/r/SARAHAI/)

[reddit.com/r/Soulnexus](https://www.reddit.com/r/Soulnexus/)

[reddit.com/r/therisingsunsociety](https://www.reddit.com/r/therisingsunsociety/)

[reddit.com/r/UnitedNames](https://www.reddit.com/r/UnitedNames/)

Renegade Dancer Productions

[rksband.com](https://www.rksband.com/)

[rksdesign.com](https://rksdesign.com/)

[robertsspaceindustries.com](https://robertsspaceindustries.com/)

[robinhood.com](https://robinhood.com/)

[rockdesign.com](https://www.rockdesign.com/)

[sabagegah.tumblr.com](https://sabagegah.tumblr.com/)

[salt.org](https://www.salt.org/)

[saltstack.com](https://www.saltstack.com/)

[sailpoint.com](https://www.sailpoint.com/)

[sambakza.net](http://sambakza.net/)

[samharris.org](https://samharris.org/)

[samsung.com](https://www.samsung.com/)

[samvak.tripod.com](http://samvak.tripod.com/)

[sbgi.net](http://sbgi.net/)

[scientology.org](https://www.scientology.org/)

[scp-wiki.net](http://www.scp-wiki.net)

[secretservice.gov](https://www.secretservice.gov/)

[seculartherapy.org](https://www.seculartherapy.org/)

[sennheiser.com](https://sennheiser.com)

[www.sheldrake.org](https://www.sheldrake.org/)

[singularitydating.com](http://www.singularitydating.com/)

[simulationseries.com](https://www.simulationseries.com/)

[sis.gov.uk](https://www.sis.gov.uk/)

[slack.com](https://slack.com/)

[spacex.com](https://www.spacex.com/)

[spotify.com](https://www.spotify.com)

[square-enix.com](https://www.square-enix.com/)

[starbucks.com](https://www.starbucks.com/)

[statefarm.com](https://www.statefarm.com/)

[steampowered.com](https://store.steampowered.com/)

[su.org](https://su.org/)

[surfshark.com](https://surfshark.com/)

[talosenergy.com](https://www.talosenergy.com/)

[targetedjustice.com](https://www.targetedjustice.com/)

[teamtrees.org](https://teamtrees.org/)

[teamviewer.com](https://www.teamviewer.com/)

[telltale.com](https://telltale.com/)

[templeos.org](https://templeos.org/)

[tesla.com](https://www.tesla.com/)

[tetradian.com](http://tetradian.com/)

[tfes.org](https://www.tfes.org/)

[theasmrcollective.com](https://www.theasmrcollective.com/)

[thecollective.com](https://www.thecollective.com/)

[theknotww.com](https://www.theknotww.com/)

[theloove.com](http://theloove.com/)

[thesatanictemple.com](https://thesatanictemple.com/)

[thethinkingatheist.com](https://www.thethinkingatheist.com/)

[thielcapital.com](http://www.thielcapital.com/)

[thisman.org](https://www.thisman.org/)

[tiktok.com](https://www.tiktok.com/)

[thetinfoilhatsociety.com](https://thetinfoilhatsociety.com/)

[toehider.bandcamp.com](https://toehider.bandcamp.com/)

[torproject.org](https://www.torproject.org/)

[tothestarsacademy.com](https://home.tothestarsacademy.com/)

[Truth Movement Australia](https://www.facebook.com/groups/37745119727/)

[tso.com](https://tso.com/)

[tubring.bandcamp.com](https://tubring.bandcamp.com/)

[twit.tv](https://twit.tv/)

[twitter.com](https://twitter.com/)

[ubisoft.com](https://www.ubisoft.com/)

[uh.edu](https://uh.edu/)

[uhd.edu](https://www.uhd.edu/)

[unitednuclear.com](https://www.unitednuclear.com/)

[vatican.va](http://www.vatican.va/)

[veeam.com](https://www.veeam.com/)

[whitehouse.gov](https://www.whitehouse.gov/)

[wizards.com](https://company.wizards.com/)

[woke.net](https://woke.net/)

[wordpress.org](https://wordpress.org/)

[wrighttechnologies.com](https://wrighttechnologies.com/)

[yangxueguo.cgsociety.org](https://yangxueguo.cgsociety.org/)

[youaredreaming.org](https://youaredreaming.org/)

(*And 6,827 unknown...*)